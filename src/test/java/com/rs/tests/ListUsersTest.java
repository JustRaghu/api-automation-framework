package com.rs.tests;
import com.rs.TestBase;
import com.rs.constants.Endpoints;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
//import org.junit.Assert;
import org.testng.Assert;
import io.restassured.path.json.JsonPath;

public class ListUsersTest extends TestBase {


    @Test
    public void testStatusCode() {

        RequestSpecification request = RestAssured.given();
        Response response = request.get(Endpoints.USERS_ENDPOINT);
        int statusCode = response.getStatusCode();
        Assert.assertEquals(statusCode, 200, "Test Passed");
    }

    @Test
    public void testUsersHeader() {
        RequestSpecification request = RestAssured.given();
        Response response = request.get(Endpoints.USERS_ENDPOINT);

        String contentType = response.header("Content-Type");
        Assert.assertEquals(contentType, "application/json; charset=UTF-8");

        // Reader header of a give name. In this line we will get
        // Header named Server
        String serverType =  response.header("Server");
        Assert.assertEquals(serverType, "Google Frontend" );

    }


    @Test
    public void testUsersResponse() {
//        given().contentType(ContentType.JSON).when().get(Endpoints.USERS_ENDPOINT).then().extract().response().prettyPrint();
        RequestSpecification request = RestAssured.given();
        Response response = request.get(Endpoints.USERS_ENDPOINT);
        JsonPath jsonPathEvaluator = response.jsonPath();

        int status = jsonPathEvaluator.get("status");
        Assert.assertEquals(status, 200, "Status is 200");

        String message = jsonPathEvaluator.get("message");
        Assert.assertEquals(message, "data retrieved successful", "Data Retrieved");

        String role = jsonPathEvaluator.get("employeeData[0].role");
        Assert.assertEquals(role, "QA Automation Developer", "Role is expected");

        int age = jsonPathEvaluator.get("employeeData[0].age");
        Assert.assertEquals(age, 25, "Age is 25");

        String dob = jsonPathEvaluator.get("employeeData[0].dob");
        Assert.assertEquals(dob, "25-02-1994", "Date is expected");
    }

    @Test
    public void testNegetive() {

        RequestSpecification request = RestAssured.given();
        Response response = request.get(Endpoints.USERS_ENDPOINT);
        JsonPath jsonPathEvaluator = response.jsonPath();

        String company = jsonPathEvaluator.get("employeeData[0].company");
        Assert.assertEquals(company, "ABC Infotech", "Date is expected");
    }

}
