package com.rs;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;

public class TestBase {

    @BeforeClass
    public void setUP() {
        TestBase.setBaseURI("http://demo4032024.mockable.io");
    }

    public static void setBaseURI(String baseURI) {
        RestAssured.baseURI = baseURI;
    }
}
